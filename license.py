# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 12:18:52 2021

@author: Patrycja
"""

import hashlib # FOR SHA1

def listToString(s): 
    str1 = "" 
    return (str1.join(s))

my_dict = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
string_my_dict = listToString(my_dict)
coding = 36

def Decrypt(hashed_text):
    output=0
    temp = 0
    for c in reversed(range(len(hashed_text))):
        znak = hashed_text[c]
        output+=string_my_dict.find(znak)*coding**temp
        temp += 1
    return output

def Encrypt(dec):
    x = (dec % coding)
    rest = dec//coding

    if (rest == 0): 
        return my_dict[int(x)]
    return Encrypt(rest) + my_dict[int(x)]

choose = input("T - Trial\nP - Permanent\n").rstrip()

if choose == "T":
    request = input("Enter request code:\n")
    request_trial_200 = "RXK" + request[3:]
    trial_200 = hashlib.sha1(request_trial_200.encode()).hexdigest()[:12] 
    
    request_trial_600 = "RX3" + request[3:]
    trial_600 = hashlib.sha1(request_trial_600.encode()).hexdigest()[:12] 

    print("\nTRIAL 200RH =",trial_200)
    print("TRIAL 600RH =",trial_600)
elif choose == "P":    
    serial_no = int(input("Enter PD688 serial no:\n"))

    int_license = serial_no * 1000000000 + 314271828
    str_license = str(Encrypt(int_license))
    permanent_license = hashlib.sha1(str_license.encode())
      
    print("\nPERMANENT =",permanent_license.hexdigest()[:12])
else: 
    pass