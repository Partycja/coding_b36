# -*- coding: utf-8 -*-
"""
Created on Fri Jul 17 10:37:42 2020

@author: Patrycja
"""

# List to string conversion
def listToString(s): 
	
	str1 = "" 
	for ele in s: 
		str1 += ele 
	return str1 

# Declare global variables
my_dict = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
string_my_dict = listToString(my_dict)
coding = 36

# ENCRYPTION
def toOur36(dec):
    x = (dec % coding)
    rest = dec//coding
    
    print("x: {0}, rest: {1}, dict: {2}".format(x,rest,my_dict[int(x)]).replace(' ','\t'))
    
    if (rest == 0):
        return my_dict[int(x)]
    return toOur36(rest) + my_dict[int(x)]

# DECRYPTION
def fromOur36(string):
    output=0
    temp = 0
    for c in reversed(range(len(string))):
        znak = string[c]
        output+=string_my_dict.find(znak)*coding**temp
        temp += 1
        print ("Char: {}, place: {}, c: {}, {}^{}: {}".format(znak,string_my_dict.find(znak),c,coding,temp,coding**temp).replace(' ','\t'))
    return output

# Main
print('Select operation: \nE: encryption\nD: decryption')
op = str(input())

if op == "E":
    print('Quint64 please:')
    quest_pre = int(input())
    print('\nCODING: {}\n'.format(coding))
    result_1 = toOur36(quest_pre) 
    print("\nQuint64 to String36: {} \n".format(str(result_1)))
if op == "D":
    print('String plese:')
    string_pre = str(input())
    result_2 = str(fromOur36(string_pre))
    print("\nString36 to Quint64: ", result_2)
    print("\nPD688SN: {}, EventCode: {}, Random/Function: {}".format(result_2[:8],result_2[8:11],result_2[11:]).replace(' ','  '))

