"""
04.06.2020
@author: Patrycja
"""
import string
import random

# List to string conversion
def listToString(s): 
	
	str1 = "" 
	for ele in s: 
		str1 += ele 
	return str1 

# Rand generator
def generator(size=5, chars=string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

# Declare global variables
my_dict = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
string_my_dict = listToString(my_dict)
coding = 36

def Decrypt(string):
    output=0
    temp = 0
    for c in reversed(range(len(string))):
        znak = string[c]
        output+=string_my_dict.find(znak)*coding**temp
        temp += 1
        #print ("Char: {}, place: {}, c: {}, {}^{}: {}".format(znak,string_my_dict.find(znak),c,coding,temp,coding**temp).replace(' ','\t'))
    return output

def Encrypt(dec):
    x = (dec % coding)
    rest = dec//coding
    
    #print("x: {0}, rest: {1}, dict: {2}".format(x,rest,my_dict[int(x)]).replace(' ','\t'))
    
    if (rest == 0): 
        return my_dict[int(x)]
    return Encrypt(rest) + my_dict[int(x)]

if __name__ == '__main__': 
    
    # # TEST- hashed sample numbers generator
    
    f = open('samples_encrypted_list.txt','w')

    for i in range(150):
        rand1 = random.randint(100, 999)
        rand2 = random.randint(10, 99)
        #print("Rand1: {}, Rand2: {}".format(rand1,rand2))
        loc_no = rand1*10000000000 + (10139486)*100 + rand2
        #print(loc_no)
        f.write(Encrypt(loc_no))
        f.write("\n")
    f.close()
    ''''''''''''
    
    filepath = 'samples_encrypted_list.txt'
    with open(filepath) as fp:
        line = fp.readline() 
        cnt = 1
        while line:
            result_2 = str(Decrypt(line.rstrip()))
            print("Sample {} Encrypted: {} Decrypted {} ".format(cnt, line.rstrip(), result_2[3:-2]).replace(' ','\t'))
                  
            line = fp.readline()
            cnt += 1